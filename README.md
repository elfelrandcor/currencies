Для разворота достаточно выполнить `docker-compose up -d --build && docker-compose logs -f front` дождаться когда завершится загрузка пакетов и миграции, отключиться от логов. Для выполнения команды обновления внутри контейнера `docker-compose exec front php yii currency/update`

В миграциях создается один пользователь с bearer токеном, он показан в примерах запросов.

Примеры запросов

```
GET http://localhost:10000/currencies
Accept: application/json
Content-Type: application/json
Authorization: Bearer $2y$10$FPsWsz5ft5YFKzfWt5HXouIleh84.ac2y6oRKoW7.wNjbVUjRuAfu

###
```

```
GET http://localhost:10000/currencies?p=2
Accept: application/json
Content-Type: application/json
Authorization: Bearer $2y$10$FPsWsz5ft5YFKzfWt5HXouIleh84.ac2y6oRKoW7.wNjbVUjRuAfu

###
```

```
GET http://localhost:10000/currency/1
Accept: application/json
Content-Type: application/json
Authorization: Bearer $2y$10$FPsWsz5ft5YFKzfWt5HXouIleh84.ac2y6oRKoW7.wNjbVUjRuAfu

###
```

