<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace common\models;


use yii\behaviors\AttributeTypecastBehavior;
use yii\db\ActiveRecord;

/**
 * Class Currency
 * @package common\models
 *
 * @property integer id
 * @property string name	Название валюты
 * @property float rate		Актуальный курс к рублю
 */
class Currency extends ActiveRecord {

	public function rules() {
		return [
			[['name', 'rate'], 'safe'],
		];
	}

	public function behaviors() {
		return [
			'typecast' => [
				'class' => AttributeTypecastBehavior::class,
				'attributeTypes' => [
					'rate' => AttributeTypecastBehavior::TYPE_FLOAT,
				],
			],
		];
	}


}