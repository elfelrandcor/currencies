<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace frontend\actions;


use common\models\Currency;

/**
 * Отображение рейта вместо полных данных о модели
 *
 * Class ViewAction
 * @package frontend\actions
 */
class ViewAction extends \yii\rest\ViewAction {

	public function run($id) {
		/** @var Currency $model */
		$model = parent::run($id);
		return $model->rate;
	}
}