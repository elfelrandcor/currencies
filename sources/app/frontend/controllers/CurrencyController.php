<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace frontend\controllers;


use common\models\Currency;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\rest\IndexAction;

class CurrencyController extends ActiveController {

	public $modelClass = Currency::class;

	public function behaviors() {
		$behaviours =  parent::behaviors();
		$behaviours['authenticator'] = [
			'class' => HttpBearerAuth::class,
		];

		return $behaviours;
	}

	public function actions()
	{
		return [
			'index' => [
				'class' => IndexAction::class,
				'modelClass' => $this->modelClass,
				'checkAccess' => [$this, 'checkAccess'],
				'prepareDataProvider' => function(IndexAction $action, $filter = null) {
					/* @var $modelClass \yii\db\BaseActiveRecord */
					$modelClass = $action->modelClass;

					/** @var ActiveQuery $query */
					$query = $modelClass::find();
					if (!empty($filter)) {
						$query->andWhere($filter);
					}

					$requestParams = Yii::$app->getRequest()->getBodyParams();
					if (empty($requestParams)) {
						$requestParams = Yii::$app->getRequest()->getQueryParams();
					}

					$pagination = new Pagination(['pageParam' => 'p', 'defaultPageSize' => 5]);

					return Yii::createObject([
						'class' => ActiveDataProvider::class,
						'query' => $query,
						'pagination' => $pagination,
						'sort' => [
							'params' => $requestParams,
						],
					]);
				},
			],
			'view' => [
				'class' => \frontend\actions\ViewAction::class,
				'modelClass' => $this->modelClass,
				'checkAccess' => [$this, 'checkAccess'],
			],
		];
	}
}