<?php

use yii\db\Migration;

/**
 * Class m190315_071200_CreateCurrencyTable
 */
class m190315_071200_CreateCurrencyTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%currency}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'rate' => $this->decimal(10, 4),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190315_071200_CreateCurrencyTable cannot be reverted.\n";

        return false;
    }
    */
}
