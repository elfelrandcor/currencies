<?php

use yii\db\Migration;

/**
 * Class m190315_080333_CreateTestUser
 */
class m190315_080333_CreateTestUser extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$user = new \common\models\User([
			'username' => 'User1',
			'email' => 'user1@test.test',
			'token' => '$2y$10$FPsWsz5ft5YFKzfWt5HXouIleh84.ac2y6oRKoW7.wNjbVUjRuAfu',
			'auth_key' => 'auth_key1',
			'password_hash' => crypt('password', 'salt'),
		]);
		$user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->truncateTable('{{%user}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190315_080333_CreateTestUser cannot be reverted.\n";

        return false;
    }
    */
}
