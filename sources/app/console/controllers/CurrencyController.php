<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace console\controllers;


use common\models\Currency;
use GuzzleHttp\Client;
use yii\console\Controller;
use yii\helpers\Console;

class CurrencyController extends Controller {

	/**
	 * Команда для обновления курсов валют
	 */
	public function actionUpdate() {
		$url = 'http://www.cbr.ru/scripts/XML_daily.asp';

		try {
			$client = new Client();
			$response = $client->get($url);

			if ($response->getStatusCode() !== 200) {
				throw new \Exception(sprintf('(%s) %s', $response->getStatusCode(), $response->getReasonPhrase()));
			}

			if (!$body = $response->getBody()->getContents()) {
				throw new \Exception('Empty body');
			}

			$finder = Currency::find();
			$xml = new \SimpleXMLElement($body);
			foreach ($xml->Valute as $cur) {
				$model = $finder->where('name = :name', ['name' => $cur->Name])->one() ?: new Currency(['name' => $cur->Name]);
				$model->rate = str_replace(',', '.', $cur->Value);

				if (!$model->save()) {
					throw new \Exception(implode(', ', $model->getFirstErrors()));
				}
			}

		} catch (\Throwable $e) {
			return $this->stdout('Error: ' . $e->getMessage() . PHP_EOL, Console::FG_RED);
		}
		return $this->stdout('Update complete' . PHP_EOL, Console::FG_GREEN);
	}
}